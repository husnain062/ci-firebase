import { NgGitlabFirebasePage } from './app.po';

describe('ng-gitlab-firebase App', () => {
  let page: NgGitlabFirebasePage;

  beforeEach(() => {
    page = new NgGitlabFirebasePage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
